# Prospects

- Public API Motor to get Finnish registered companies from the Finnish Patent and Registration Office.

- **Step one:** Build time range based search, eg: City: Helsinki, start: 01-01-2014, End: 31-01-2014
- Public APi has limit for single request, so best way is limit timeframe per month.
- This is start App, only made APi Process so far and extract JSON infomation.
- There is no nice UI yet ;-D


- Process start from URL /search - there is simple form
- which sent all request data to GetDataController, store method.
- Store method makes verification and pass data to Prh model.

- **Step two:** Prh model pass information to API request (PRH) by using GuzzleHttp.
- This response contains only Company name and businessId(vat code).
- Model collect information and makes second request to API via PRHData model, getdetails function.
- API Response includes all data about Company, I have limit request only OY (Ltd) companies.

- Main model: PRHData has child models: Addr, BLines, ProsContact, ProsRegisters
- main issue is : **How to return data from child model back to main one ?**


- Eg.
`  public static function businessLines($company){

      foreach ($company->businessLines as $businessLines) {

              $Code = $businessLines->code;
              $BlLanguage = $businessLines->language;
              $BLnameFI = $company->businessLines[1]->name;
              $BLnameSE = $company->businessLines[2]->name;
              $BLnameEN = $company->businessLines[0]->name;

              Arr::set($Prospect, 'Bcode',$Code);
              Arr::set($Prospect, 'Bname',$BLnameEN);

              //echo "Company Category :  " . $BLnameEN . '<br/>' ;

              // dump($Prospect);
              return $Prospect;
                } // End of foreach`

- I want
`return $Prospect [
    'Bcode',$Code
    'Bname',$BLnameEN
]`

- So I can add them to main main model database save process
'  $NewCompany = Pros::updateOrCreate($Prospect,[

        ['businessId', $businessId],
        ['name',$name],
        ['form',$companyForm],
        ['Bcode',$code]
        ['Bname',$BLinesEN]'
